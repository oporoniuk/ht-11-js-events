// Теоретичні питання
// 1. Що таке події в JavaScript і для чого вони використовуються?
// Подіі - це сигнал того, що щось сталося.
//Для реагування на події ми можемо призначити обробник - функцію, яка виконується в разі події.


// 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
// Події миші:
// click - коли миша клікає на елемент (на сенсорних пристроях це генерується при торканні).
// contextmenu - коли користувач правою кнопкою миші клікає на елемент.
// mouseover / mouseout - коли курсор миші наводиться / залишається на елементі.
// mousedown / mouseup - коли кнопка миші натискана / відпускається над елементом.
// mousemove - коли миша рухається.

// 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
// contextmenu - коли користувач правою кнопкою миші клікає на елемент.
//Існують інші способи відкрити контекстне меню, наприклад, за допомогою спеціальної клавіші на клавіатурі; 
//в цьому випадку також спрацьовує ця подія, тому вона не є виключно подією миші.




// Практичні завдання
// 1. Додати новий абзац по кліку на кнопку:

// По кліку на кнопку <button id="btn-click">Click Me</button>, 
//створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const content = document.getElementById('content');
const button = document.getElementById('btn-click');
button.addEventListener('click', () => {
    const paragraph = document.createElement('p');
    paragraph.textContent = "New Paragraph";
    
    content.prepend(paragraph);
});







// 2. Додати новий елемент форми із атрибутами:

// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.

// По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути,
// наприклад, type, placeholder, і name. та додайте його під кнопкою.

const createInputBtn = document.createElement('button');
createInputBtn.setAttribute('id', 'btn-input-create');
createInputBtn.textContent = 'Create Input';
content.append(createInputBtn);

createInputBtn.addEventListener('click', () => {
    const input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'Name');
    input.setAttribute('name', 'username');

    content.append(input);
});
